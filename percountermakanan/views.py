from django.shortcuts import render, redirect
from .forms import counterFeedback
from .models import *

# Create your views here.
def counterBatagorSiomay(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseBatagorSiomay()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/food/batagor/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseBatagorSiomay.objects.all().order_by('-id')
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percountermakanan/counterBatagorSiomay.html', response)

def counterGorengan(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseGorengan()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/food/gorengan/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseGorengan.objects.all().order_by('-id')
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percountermakanan/counterGorengan.html', response)

def counterIsabella(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseIsabella()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/food/isabella/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseIsabella.objects.all().order_by('-id')
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percountermakanan/counterIsabella.html', response)

def counterGadoGado(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseGadoGado()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/food/gadogado/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseGadoGado.objects.all().order_by('-id')
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percountermakanan/counterIstrinyaMasIpan.html', response)

def counterSoto(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseSoto()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/food/soto/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseSoto.objects.all().order_by('-id')
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percountermakanan/counterMasIpan.html', response)

def counterMasJeb(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseMasJeb()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/food/masjeb/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseMasJeb.objects.all().order_by('-id')
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percountermakanan/counterMasjeb.html', response)

def counterMasYono(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseMasYono()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/food/masyono/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseMasYono.objects.all().order_by('-id')
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percountermakanan/counterMasYono.html', response)

def counterMieAyamBaso(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseMieAyamBaso()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/food/mieayambaso/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseMieAyamBaso.objects.all().order_by('-id')
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percountermakanan/counterMieAyamBaso.html', response)

def counterNasiGoreng(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseNasiGoreng()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/food/nasgor/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseNasiGoreng.objects.all().order_by('-id')
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percountermakanan/counterNasiGoreng.html', response)

def counterNasiKuning(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseNasiKuning()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/food/nasikuning/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseNasiKuning.objects.all().order_by('-id')
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percountermakanan/counterNasiKuning.html', response)

def counterNasiPecelAyam(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseNasiPecelAyam()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/food/nasipecelsatu/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseNasiPecelAyam.objects.all().order_by('-id')
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percountermakanan/counterNasiPecelAyam.html', response)

def counterNasiPecelAyam2(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseNasiPecelAyam2()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/food/nasipeceldua/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseNasiPecelAyam2.objects.all().order_by('-id')
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percountermakanan/counterNasiPecelAyam(2).html', response)

def counterSate(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseSate()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/food/sate/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseSate.objects.all().order_by('-id')
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percountermakanan/counterSate.html', response)

def counterSotoBogor(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseSotoBogor()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/food/sotobogor/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseSotoBogor.objects.all().order_by('-id')
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percountermakanan/counterSotoBogor.html', response)

def counterWarungKelontong(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseWarungKelontong()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/food/warungkelontong/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseWarungKelontong.objects.all().order_by('-id')
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percountermakanan/counterWarungKelontong.html', response)

def counterDreamWaffle(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseDreamWaffle()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/food/waffle/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseDreamWaffle.objects.all().order_by('-id')
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percountermakanan/counterDreamWaffle.html', response)

def counterYoshinoya(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseYoshinoya()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/food/yoshi/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseYoshinoya.objects.all().order_by('-id')
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percountermakanan/counterYoshinoya.html', response)