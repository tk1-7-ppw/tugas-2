from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import *
from .forms import counterFeedback

class TestingUrls(TestCase):


    def test_apakah_url_batagor_yang_diakses_ada(self):
        response = Client().get("/counters/food/batagor/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_batagor_mengakses_function_yang_benar(self):
        found = resolve('/counters/food/batagor/')
        self.assertEqual(found.func, counterBatagorSiomay)

    def test_apakah_url_gorengan_yang_diakses_ada(self):
        response = Client().get("/counters/food/gorengan/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_gorengan_mengakses_function_yang_benar(self):
        found = resolve('/counters/food/gorengan/')
        self.assertEqual(found.func, counterGorengan)

    def test_apakah_url_isabella_yang_diakses_ada(self):
        response = Client().get("/counters/food/isabella/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_isabella_mengakses_function_yang_benar(self):
        found = resolve('/counters/food/isabella/')
        self.assertEqual(found.func, counterIsabella)

    def test_apakah_url_gado_yang_diakses_ada(self):
        response = Client().get("/counters/food/gadogado/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_gado_mengakses_function_yang_benar(self):
        found = resolve('/counters/food/gadogado/')
        self.assertEqual(found.func, counterGadoGado)

    def test_apakah_url_soto_yang_diakses_ada(self):
        response = Client().get("/counters/food/soto/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_soto_mengakses_function_yang_benar(self):
        found = resolve('/counters/food/soto/')
        self.assertEqual(found.func, counterSoto)

    def test_apakah_url_masjeb_yang_diakses_ada(self):
        response = Client().get("/counters/food/masjeb/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_masjeb_mengakses_function_yang_benar(self):
        found = resolve('/counters/food/masjeb/')
        self.assertEqual(found.func, counterMasJeb)

    def test_apakah_url_masyono_yang_diakses_ada(self):
        response = Client().get("/counters/food/masyono/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_masyono_mengakses_function_yang_benar(self):
        found = resolve('/counters/food/masyono/')
        self.assertEqual(found.func, counterMasYono)

    def test_apakah_url_mieayam_yang_diakses_ada(self):
        response = Client().get("/counters/food/mieayambaso/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_mieayam_mengakses_function_yang_benar(self):
        found = resolve('/counters/food/mieayambaso/')
        self.assertEqual(found.func, counterMieAyamBaso)

    def test_apakah_url_nasgor_yang_diakses_ada(self):
        response = Client().get("/counters/food/nasgor/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_nasgor_mengakses_function_yang_benar(self):
        found = resolve('/counters/food/nasgor/')
        self.assertEqual(found.func, counterNasiGoreng)

    def test_apakah_url_nasikuning_yang_diakses_ada(self):
        response = Client().get("/counters/food/nasikuning/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_nasikuning_mengakses_function_yang_benar(self):
        found = resolve('/counters/food/nasikuning/')
        self.assertEqual(found.func, counterNasiKuning)

    def test_apakah_url_ayam1_yang_diakses_ada(self):
        response = Client().get("/counters/food/nasipecelsatu/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_ayam1_mengakses_function_yang_benar(self):
        found = resolve('/counters/food/nasipecelsatu/')
        self.assertEqual(found.func, counterNasiPecelAyam)

    def test_apakah_url_ayam2_yang_diakses_ada(self):
        response = Client().get("/counters/food/nasipeceldua/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_ayam2_mengakses_function_yang_benar(self):
        found = resolve('/counters/food/nasipeceldua/')
        self.assertEqual(found.func, counterNasiPecelAyam2)

    def test_apakah_url_sate_yang_diakses_ada(self):
        response = Client().get("/counters/food/sate/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_sate_mengakses_function_yang_benar(self):
        found = resolve('/counters/food/sate/')
        self.assertEqual(found.func, counterSate)

    def test_apakah_url_sotobogor_yang_diakses_ada(self):
        response = Client().get("/counters/food/sotobogor/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_sotobogor_mengakses_function_yang_benar(self):
        found = resolve('/counters/food/sotobogor/')
        self.assertEqual(found.func, counterSotoBogor)

    def test_apakah_url_warung_yang_diakses_ada(self):
        response = Client().get("/counters/food/warungkelontong/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_warung_mengakses_function_yang_benar(self):
        found = resolve('/counters/food/warungkelontong/')
        self.assertEqual(found.func, counterWarungKelontong)

    def test_apakah_url_waffle_yang_diakses_ada(self):
        response = Client().get("/counters/food/waffle/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_waffle_mengakses_function_yang_benar(self):
        found = resolve('/counters/food/waffle/')
        self.assertEqual(found.func, counterDreamWaffle)

    def test_apakah_url_yoshi_yang_diakses_ada(self):
        response = Client().get("/counters/food/yoshi/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_yoshi_mengakses_function_yang_benar(self):
        found = resolve('/counters/food/yoshi/')
        self.assertEqual(found.func, counterYoshinoya)

class TestingModels(TestCase):

    def test_apakah_bisa_membuat_object_1(self):
        objectBaru = counterDatabaseBatagorSiomay.objects.create(commentM = "TESTING")
        jumlahObjectYangAda = counterDatabaseBatagorSiomay.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 1)

    def test_apakah_bisa_membuat_object_2(self):
        objectBaru = counterDatabaseGorengan.objects.create(commentM = "TESTING")
        jumlahObjectYangAda = counterDatabaseGorengan.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 1)

    def test_apakah_bisa_membuat_object_3(self):
        objectBaru = counterDatabaseIsabella.objects.create(commentM = "TESTING")
        jumlahObjectYangAda = counterDatabaseIsabella.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 1)

    def test_apakah_bisa_membuat_object_4(self):
        objectBaru = counterDatabaseGadoGado.objects.create(commentM = "TESTING")
        jumlahObjectYangAda = counterDatabaseGadoGado.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 1)

    def test_apakah_bisa_membuat_object_5(self):
        objectBaru = counterDatabaseSoto.objects.create(commentM = "TESTING")
        jumlahObjectYangAda = counterDatabaseSoto.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 1)

    def test_apakah_bisa_membuat_object_6(self):
        objectBaru = counterDatabaseMasJeb.objects.create(commentM = "TESTING")
        jumlahObjectYangAda = counterDatabaseMasJeb.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 1)

    def test_apakah_bisa_membuat_object_7(self):
        objectBaru = counterDatabaseMasYono.objects.create(commentM = "TESTING")
        jumlahObjectYangAda = counterDatabaseMasYono.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 1)

    def test_apakah_bisa_membuat_object_8(self):
        objectBaru = counterDatabaseMieAyamBaso.objects.create(commentM = "TESTING")
        jumlahObjectYangAda = counterDatabaseMieAyamBaso.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 1)

    def test_apakah_bisa_membuat_object_9(self):
        objectBaru = counterDatabaseNasiGoreng.objects.create(commentM = "TESTING")
        jumlahObjectYangAda = counterDatabaseNasiGoreng.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 1)

    def test_apakah_bisa_membuat_object_10(self):
        objectBaru = counterDatabaseNasiKuning.objects.create(commentM = "TESTING")
        jumlahObjectYangAda = counterDatabaseNasiKuning.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 1)

    def test_apakah_bisa_membuat_object_11(self):
        objectBaru = counterDatabaseNasiPecelAyam.objects.create(commentM = "TESTING")
        jumlahObjectYangAda = counterDatabaseNasiPecelAyam.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 1)

    def test_apakah_bisa_membuat_object_12(self):
        objectBaru = counterDatabaseNasiPecelAyam2.objects.create(commentM = "TESTING")
        jumlahObjectYangAda = counterDatabaseNasiPecelAyam2.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 1)

    def test_apakah_bisa_membuat_object_13(self):
        objectBaru = counterDatabaseSate.objects.create(commentM = "TESTING")
        jumlahObjectYangAda = counterDatabaseSate.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 1)

    def test_apakah_bisa_membuat_object_14(self):
        objectBaru = counterDatabaseSotoBogor.objects.create(commentM = "TESTING")
        jumlahObjectYangAda = counterDatabaseSotoBogor.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 1)

    def test_apakah_bisa_membuat_object_15(self):
        objectBaru = counterDatabaseWarungKelontong.objects.create(commentM = "TESTING")
        jumlahObjectYangAda = counterDatabaseWarungKelontong.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 1)

    def test_apakah_bisa_membuat_object_16(self):
        objectBaru = counterDatabaseDreamWaffle.objects.create(commentM = "TESTING")
        jumlahObjectYangAda = counterDatabaseDreamWaffle.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 1)

    def test_apakah_bisa_membuat_object_17(self):
        objectBaru = counterDatabaseYoshinoya.objects.create(commentM = "TESTING")
        jumlahObjectYangAda = counterDatabaseYoshinoya.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 1)

class TestingViews(TestCase):

    def test_apakah_bisa_post_1(self):
        test = 'Unknown'
        response_post = Client().post("/counters/food/batagor/", {"Let_us_know_your_opinion_about_this_counter" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_blank_1(self):
        form = counterFeedback(data={"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Let_us_know_your_opinion_about_this_counter'], ['This field is required.'])

    def test_apakah_bisa_post_2(self):
        test = 'Unknown'
        response_post = Client().post("/counters/food/gorengan/", {"Let_us_know_your_opinion_about_this_counter" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_blank_2(self):
        form = counterFeedback(data={"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Let_us_know_your_opinion_about_this_counter'], ['This field is required.'])

    def test_apakah_bisa_post_3(self):
        test = 'Unknown'
        response_post = Client().post("/counters/food/isabella/", {"Let_us_know_your_opinion_about_this_counter" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_blank_3(self):
        form = counterFeedback(data={"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Let_us_know_your_opinion_about_this_counter'], ['This field is required.'])

    def test_apakah_bisa_post_4(self):
        test = 'Unknown'
        response_post = Client().post("/counters/food/gadogado/", {"Let_us_know_your_opinion_about_this_counter" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_blank_4(self):
        form = counterFeedback(data={"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Let_us_know_your_opinion_about_this_counter'], ['This field is required.'])

    def test_apakah_bisa_post_5(self):
        test = 'Unknown'
        response_post = Client().post("/counters/food/soto/", {"Let_us_know_your_opinion_about_this_counter" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_blank_5(self):
        form = counterFeedback(data={"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Let_us_know_your_opinion_about_this_counter'], ['This field is required.'])

    def test_apakah_bisa_post_6(self):
        test = 'Unknown'
        response_post = Client().post("/counters/food/masjeb/", {"Let_us_know_your_opinion_about_this_counter" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_blank_6(self):
        form = counterFeedback(data={"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Let_us_know_your_opinion_about_this_counter'], ['This field is required.'])

    def test_apakah_bisa_post_7(self):
        test = 'Unknown'
        response_post = Client().post("/counters/food/masyono/", {"Let_us_know_your_opinion_about_this_counter" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_blank_7(self):
        form = counterFeedback(data={"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Let_us_know_your_opinion_about_this_counter'], ['This field is required.'])

    def test_apakah_bisa_post_8(self):
        test = 'Unknown'
        response_post = Client().post("/counters/food/mieayambaso/", {"Let_us_know_your_opinion_about_this_counter" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_blank_8(self):
        form = counterFeedback(data={"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Let_us_know_your_opinion_about_this_counter'], ['This field is required.'])

    def test_apakah_bisa_post_9(self):
        test = 'Unknown'
        response_post = Client().post("/counters/food/nasgor/", {"Let_us_know_your_opinion_about_this_counter" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_blank_9(self):
        form = counterFeedback(data={"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Let_us_know_your_opinion_about_this_counter'], ['This field is required.'])

    def test_apakah_bisa_post_10(self):
        test = 'Unknown'
        response_post = Client().post("/counters/food/nasikuning/", {"Let_us_know_your_opinion_about_this_counter" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_blank_10(self):
        form = counterFeedback(data={"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Let_us_know_your_opinion_about_this_counter'], ['This field is required.'])

    def test_apakah_bisa_post_11(self):
        test = 'Unknown'
        response_post = Client().post("/counters/food/nasipecelsatu/", {"Let_us_know_your_opinion_about_this_counter" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_blank_11(self):
        form = counterFeedback(data={"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Let_us_know_your_opinion_about_this_counter'], ['This field is required.'])

    def test_apakah_bisa_post_12(self):
        test = 'Unknown'
        response_post = Client().post("/counters/food/nasipeceldua/", {"Let_us_know_your_opinion_about_this_counter" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_blank_12(self):
        form = counterFeedback(data={"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Let_us_know_your_opinion_about_this_counter'], ['This field is required.'])

    def test_apakah_bisa_post_13(self):
        test = 'Unknown'
        response_post = Client().post("/counters/food/sate/", {"Let_us_know_your_opinion_about_this_counter" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_blank_13(self):
        form = counterFeedback(data={"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Let_us_know_your_opinion_about_this_counter'], ['This field is required.'])

    def test_apakah_bisa_post_14(self):
        test = 'Unknown'
        response_post = Client().post("/counters/food/sotobogor/", {"Let_us_know_your_opinion_about_this_counter" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_blank_14(self):
        form = counterFeedback(data={"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Let_us_know_your_opinion_about_this_counter'], ['This field is required.'])

    def test_apakah_bisa_post_15(self):
        test = 'Unknown'
        response_post = Client().post("/counters/food/warungkelontong/", {"Let_us_know_your_opinion_about_this_counter" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_blank_15(self):
        form = counterFeedback(data={"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Let_us_know_your_opinion_about_this_counter'], ['This field is required.'])

    def test_apakah_bisa_post_16(self):
        test = 'Unknown'
        response_post = Client().post("/counters/food/waffle/", {"Let_us_know_your_opinion_about_this_counter" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_blank_16(self):
        form = counterFeedback(data={"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Let_us_know_your_opinion_about_this_counter'], ['This field is required.'])

    def test_apakah_bisa_post_17(self):
        test = 'Unknown'
        response_post = Client().post("/counters/food/yoshi/", {"Let_us_know_your_opinion_about_this_counter" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_blank_17(self):
        form = counterFeedback(data={"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Let_us_know_your_opinion_about_this_counter'], ['This field is required.'])