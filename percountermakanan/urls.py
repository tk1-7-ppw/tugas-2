from django.urls import path
from .views import *

urlpatterns = [
    path('food/batagor/', counterBatagorSiomay),
    path('food/gorengan/', counterGorengan),
    path('food/isabella/', counterIsabella),
    path('food/gadogado/', counterGadoGado),
    path('food/soto/', counterSoto),
    path('food/masjeb/', counterMasJeb),
    path('food/masyono/', counterMasYono),
    path('food/mieayambaso/', counterMieAyamBaso),
    path('food/nasgor/', counterNasiGoreng),
    path('food/nasikuning/', counterNasiKuning),
    path('food/nasipecelsatu/', counterNasiPecelAyam),
    path('food/nasipeceldua/', counterNasiPecelAyam2),
    path('food/sate/', counterSate),
    path('food/sotobogor/', counterSotoBogor),
    path('food/warungkelontong/', counterWarungKelontong),
    path('food/waffle/', counterDreamWaffle),
    path('food/yoshi/', counterYoshinoya),
]

