from django.db import models

# Create your models here.
class counterDatabaseBatagorSiomay(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseGorengan(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseIsabella(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseGadoGado(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseSoto(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseMasJeb(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseMasYono(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseMieAyamBaso(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseNasiGoreng(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseNasiKuning(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseNasiPecelAyam(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseNasiPecelAyam2(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseSate(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    
class counterDatabaseSotoBogor(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseWarungKelontong(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseDreamWaffle(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseYoshinoya(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class databaseMakanan(models.Model) :
    nama_makanan = models.CharField(blank=False, max_length=100)
    terdapat_di_counter = models.CharField(blank=False, max_length=100)
    deskripsi = models.TextField(blank=False)
    harga = models.DecimalField(blank=False, max_digits=9, decimal_places=3)
    created_at = models.DateTimeField(auto_now_add=True)