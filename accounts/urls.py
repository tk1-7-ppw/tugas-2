from django.urls import path
from . import views

urlpatterns = [
    path('SignUp/', views.signup_views),
    path('SignIn/', views.signin_views),
    path('SignOut/', views.signout_views),
]