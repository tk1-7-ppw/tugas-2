# Tugas Kelompok 1
This Gitlab repository is the result of the work from Muhammad Luthfi Idrus, Naufal Hilmi Irfandi, Mohammad Al Kwarizmi Dwi Anggara, Raihan Miftah Musefi Saputra.

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/tk1-7-ppw/tugas-2/badges/master/pipeline.svg)](https://gitlab.com/tk1-7-ppw/tugas-2/commits/master)
[![coverage report](https://gitlab.com/tk1-7-ppw/tugas-2/badges/master/coverage.svg)](https://gitlab.com/tk1-7-ppw/tugas-2/commits/master)

## Heroku
Click [here](http://fasfood.herokuapp.com) to see my website.

## Project Member
- Muhammad Luthfi Idrus, 1806141353
- Naufal Hilmi Irfandi, 1806186673
- Mohammed Al Kwarizmi Dwi Anggara, 1806185405
- Raihan Miftah Musefi Saputra, 1806191055