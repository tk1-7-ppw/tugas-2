from django.urls import path
from .views import feedbackViews

urlpatterns = [
    path('', feedbackViews),
]