from django.shortcuts import render, redirect
from . import forms, models

def feedbackViews(request):
    if(request.method == "POST"):
        tmp = forms.feedback(request.POST)
        if request.user.is_authenticated:
            tmp2 = models.feedbackDatabase()
            tmp2.nama = request.user
            tmp2.cerita = tmp["Your_Story"].value()
        else:
            if(tmp.is_valid()):
                tmp2 = models.feedbackDatabase()
                tmp2.nama = tmp.cleaned_data["Name"]
                tmp2.cerita = tmp.cleaned_data["Your_Story"]
        tmp2.save()
        return redirect("/feedback")
    else:
        tmp = forms.feedback()
        tmp2 = models.feedbackDatabase.objects.all()
        if request.user.is_authenticated:
            tmp_dictio = {
                'form' : tmp["Your_Story"],
                'feedback' : tmp2
            }
        else:
            tmp_dictio = {
                'form' : tmp,
                'feedback' : tmp2
            }
        return render(request, 'feedback/feedback.html', tmp_dictio)