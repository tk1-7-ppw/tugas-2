from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.contrib.auth.models import User
from .views import feedbackViews
from .models import feedbackDatabase
from .forms import feedback
import unittest

# Create your tests here.
class TestingUrls(TestCase):

    def test_apakah_url_yang_diakses_ada(self):
        response = Client().get("/feedback/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_mengakses_function_yang_benar(self):
        found = resolve('/feedback/')
        self.assertEqual(found.func, feedbackViews)

class TestingModels(TestCase):

    def test_apakah_bisa_membuat_object(self):
        objectBaru = feedbackDatabase.objects.create(nama = "TESTING")
        objectBaru2 = feedbackDatabase.objects.create(cerita = "TESTING")
        jumlahObjectYangAda = feedbackDatabase.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 2)

class UnitTest(TestCase):

    def test_apakah_bisa_nama_dan_story_bisa_dipost(self):
        test = 'Unknown'
        response_post = Client().post("/feedback/", {"Name" : test, "Your_Story" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_nama_blank(self):
        form = feedback(data={"Name" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Name'], ['This field is required.'])
    
    def test_apakah_bisa_post_jika_cerita_blank(self):
        form = feedback(data={"Your_Story" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Your_Story'], ['This field is required.'])
        
    def test_apakah_inputan_muncul_di_layar(self):
        response = Client().post('/feedback/', {'Name': "al", 'Your_Story': "keren"})
        new_response = Client().get("/feedback/")
        html_response = new_response.content.decode("utf8")
        self.assertIn("al", html_response)
        self.assertIn("keren", html_response)

    def test_apakah_bisa_nama_dan_story_bisa_dipost_oleh_user1(self):
        people = Client()
        people.login(username = "naufalirfandi", password = "irfandi123")
        test = 'Unknown'
        test2 = 'none'
        response_post = people.post("/feedback/", {'Name': test2, 'Your_Story': test})
        self.assertEqual(response_post.status_code, 302)