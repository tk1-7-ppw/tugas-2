from django.db import models

# Create your models here.

class Makanan(models.Model):
    nama = models.CharField(blank=False, max_length=1000)
    harga = models.IntegerField(blank=False)
    deskripsi = models.TextField(blank=False)
    counter = models.CharField(blank=False, max_length=1000)
    url = models.CharField(max_length=50, default="ganti")

class Minuman(models.Model):
    nama = models.CharField(blank=False, max_length=1000)
    harga = models.IntegerField(blank=False)
    deskripsi = models.TextField(blank=False)
    counter = models.CharField(blank=False, max_length=1000)
    url = models.CharField(max_length=50, default="ganti")

class Counter(models.Model):
    tipe = models.CharField(blank=False, max_length=50)
    nama = models.CharField(blank=False, max_length=1000)
    url = models.CharField(max_length=100, default="ganti")