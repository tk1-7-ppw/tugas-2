from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'makananAPI', views.MakananViewSet)
router.register(r'minumanAPI', views.MinumanViewSet)
router.register(r'counterAPI', views.CounterViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]