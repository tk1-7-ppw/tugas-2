from rest_framework import serializers
from .models import Makanan, Minuman, Counter

class MakananSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Makanan
        fields = ("nama", "harga", "deskripsi", "counter", "url")

class MinumanSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Minuman
        fields = ("nama", "harga", "deskripsi", "counter", "url")

class CounterSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Counter
        fields = ("tipe", "nama", "url")