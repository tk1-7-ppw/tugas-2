from django.shortcuts import render
from rest_framework import viewsets
from .serializers import MakananSerializer, MinumanSerializer, CounterSerializer
from .models import Makanan, Minuman, Counter


class MakananViewSet(viewsets.ModelViewSet):
    queryset = Makanan.objects.all()
    serializer_class = MakananSerializer

class MinumanViewSet(viewsets.ModelViewSet):
    queryset = Minuman.objects.all()
    serializer_class = MinumanSerializer

class CounterViewSet(viewsets.ModelViewSet):
    queryset = Counter.objects.all()
    serializer_class = CounterSerializer