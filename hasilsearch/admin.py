from django.contrib import admin
from .models import Makanan, Minuman, Counter
# Register your models here.

admin.site.register(Makanan)
admin.site.register(Minuman)
admin.site.register(Counter)