$(document).ready(function() {
    $.ajax({
        url: "http://fasfood.herokuapp.com/search/counterAPI/?format=json",
        success: function(data) {
            $('.container').html('')
            var result = '';
            var index = 0;
            for (var i = 0; i < data.length; i++) {
                if (data[i].tipe == "minuman") {
                    if (index % 2 == 0) {
                        result += `</div>`;
                        result += `<div class='row'>`;
                    }
                    result += `<div class='col'><div class='row tengah'><div class='col'><div class='photo'><img src='/static/MINUMAN.jfif' alt=\"Drink Counter\"></div></div><div class=\"col\"><div class=\"tulisanbutton justify-content-center\"><div class=\"nama\">`;
                    result += data[i].nama;
                    result += `</div>`;
                    result += "<a href='" + data[i].url + "'><button type='button' class='btn btn-warning'>See More</button>";
                    result += "</a>";
                    result += `</div></div></div></div>`;
                    index++;
                }
            }
            $('.container').append(result);
        },
        error: function(error) {
            alert("GAGAL DAPET");
        }
    });
});