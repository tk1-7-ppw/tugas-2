from django import forms

class websiteComments(forms.Form) :
    Let_us_know_your_opinion_about_this_web = forms.CharField(widget=forms.Textarea(attrs={
        'class' : 'form-control',
        'placeholder' : 'Type your opinion here!',
        'type' : 'text',
        'required' : True
    }))