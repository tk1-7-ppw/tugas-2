from django.test import TestCase, Client
from django.urls import resolve
from .views import homepage
from .models import homepageDatabase
from .forms import websiteComments

class TestingUrls(TestCase):

    def test_apakah_url_yang_diakses_ada(self):
        response = Client().get("/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_mengakses_function_yang_benar(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)

class TestingModels(TestCase):

    def test_apakah_bisa_membuat_object(self):
        objectBaru = homepageDatabase.objects.create(commentM = "TESTING")
        jumlahObjectYangAda = homepageDatabase.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 1)

class TestingViews(TestCase):

    def test_apakah_bisa_post(self):
        test = 'Unknown'
        response_post = Client().post("/", {"Let_us_know_your_opinion_about_this_web" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_blank(self):
        form = websiteComments(data={"Let_us_know_your_opinion_about_this_web" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Let_us_know_your_opinion_about_this_web'], ['This field is required.'])