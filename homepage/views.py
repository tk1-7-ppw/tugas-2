from django.shortcuts import render, redirect
from .forms import websiteComments
from .models import homepageDatabase
from percountermakanan.models import databaseMakanan
from percounterminuman.models import databaseMinuman

# Create your views here.
def homepage(request) :
    if (request.method == 'POST') :
        form = websiteComments(request.POST)
        if (form.is_valid()) :
            newData = homepageDatabase()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_web']
            newData.save()
        return redirect('/')
    else :
        form = websiteComments()
        allComments = homepageDatabase.objects.all()
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'homepage/homepage.html', response)