from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import *
from .forms import counterFeedback


class TestingMasYono(TestCase):

    def test_url_nya_bisa_diakses_ga(self):
        test_url = Client().get("/counters/drink/masyono/")
        self.assertEqual(test_url.status_code, 200)

    def test_fungsi_nya_bisa_ga(self):
        function = resolve("/counters/drink/masyono/")
        self.assertEqual(function.func, counterMasYono)

    def test_bisa_ngepost_ga(self):
        tes = "Unknown"
        respon = Client().post("/counters/drink/masyono/", {"Let_us_know_your_opinion_about_this_counter" : tes})
        self.assertEqual(respon.status_code, 302)

    def test_kalo_gaisi_apa_apa_bis_ga(self):
        form = counterFeedback(data = {"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors["Let_us_know_your_opinion_about_this_counter"], ["This field is required."])


class TestingEsKelapa(TestCase):

    def test_url_nya_bisa_diakses_ga(self):
        test_url = Client().get("/counters/drink/eskelapa/")
        self.assertEqual(test_url.status_code, 200)

    def test_fungsi_nya_bisa_ga(self):
        function = resolve('/counters/drink/eskelapa/')
        self.assertEqual(function.func, counterEsKelapa)

    def test_bisa_ngepost_ga(self):
        tes = "Unknown"
        respon = Client().post("/counters/drink/eskelapa/", {"Let_us_know_your_opinion_about_this_counter" : tes})
        self.assertEqual(respon.status_code, 302)

    def test_kalo_gaisi_apa_apa_bis_ga(self):
        form = counterFeedback(data = {"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors["Let_us_know_your_opinion_about_this_counter"], ["This field is required."])

class TestingMinumBoss(TestCase):

    def test_url_nya_bisa_diakses_ga(self):
        test_url = Client().get("/counters/drink/minumboss/")
        self.assertEqual(test_url.status_code, 200)

    def test_fungsi_nya_bisa_ga(self):
        function = resolve('/counters/drink/minumboss/')
        self.assertEqual(function.func, counterMinumBoss)

    def test_bisa_ngepost_ga(self):
        tes = "Unknown"
        respon = Client().post("/counters/drink/minumboss/", {"Let_us_know_your_opinion_about_this_counter" : tes})
        self.assertEqual(respon.status_code, 302)

    def test_kalo_gaisi_apa_apa_bis_ga(self):
        form = counterFeedback(data = {"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors["Let_us_know_your_opinion_about_this_counter"], ["This field is required."])

class TestingMinumanKemasan(TestCase):

    def test_url_nya_bisa_diakses_ga(self):
        test_url = Client().get("/counters/drink/minumankemasan/")
        self.assertEqual(test_url.status_code, 200)

    def test_fungsi_nya_bisa_ga(self):
        function = resolve('/counters/drink/minumankemasan/')
        self.assertEqual(function.func, counterMinumanKemasan)

    def test_bisa_ngepost_ga(self):
        tes = "Unknown"
        respon = Client().post("/counters/drink/minumankemasan/", {"Let_us_know_your_opinion_about_this_counter" : tes})
        self.assertEqual(respon.status_code, 302)

    def test_kalo_gaisi_apa_apa_bis_ga(self):
        form = counterFeedback(data = {"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors["Let_us_know_your_opinion_about_this_counter"], ["This field is required."])

class TestingMinumanSachet(TestCase):

    def test_url_nya_bisa_diakses_ga(self):
        test_url = Client().get("/counters/drink/minumansachet/")
        self.assertEqual(test_url.status_code, 200)

    def test_fungsi_nya_bisa_ga(self):
        function = resolve('/counters/drink/minumansachet/')
        self.assertEqual(function.func, counterMinumanSachet)

    def test_bisa_ngepost_ga(self):
        tes = "Unknown"
        respon = Client().post("/counters/drink/minumansachet/", {"Let_us_know_your_opinion_about_this_counter" : tes})
        self.assertEqual(respon.status_code, 302)

    def test_kalo_gaisi_apa_apa_bis_ga(self):
        form = counterFeedback(data = {"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors["Let_us_know_your_opinion_about_this_counter"], ["This field is required."])

class TestingYoshinoya(TestCase):

    def test_url_nya_bisa_diakses_ga(self):
        test_url = Client().get("/counters/drink/yoshinoya/")
        self.assertEqual(test_url.status_code, 200)

    def test_fungsi_nya_bisa_ga(self):
        function = resolve('/counters/drink/yoshinoya/')
        self.assertEqual(function.func, counterYoshinoya)

    def test_bisa_ngepost_ga(self):
        tes = "Unknown"
        respon = Client().post("/counters/drink/yoshinoya/", {"Let_us_know_your_opinion_about_this_counter" : tes})
        self.assertEqual(respon.status_code, 302)

    def test_kalo_gaisi_apa_apa_bis_ga(self):
        form = counterFeedback(data = {"Let_us_know_your_opinion_about_this_counter" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors["Let_us_know_your_opinion_about_this_counter"], ["This field is required."])