from django.db import models

# Create your models here.

class counterDatabaseMasYono(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseMinumBoss(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseEsKelapa(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseMinumanKemasan(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseMinumanSachet(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class counterDatabaseYoshinoya(models.Model) :
    commentM = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

class databaseMinuman(models.Model) :
    nama_minuman = models.CharField(blank=False, max_length=100)
    terdapat_di_counter = models.CharField(blank=False, max_length=100)
    deskripsi = models.TextField(blank=False)
    harga = models.DecimalField(blank=False, max_digits=9, decimal_places=3)