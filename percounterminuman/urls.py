from django.urls import path
from .views import *

urlpatterns = [
    path('drink/masyono/', counterMasYono),
    path('drink/minumboss/', counterMinumBoss),
    path('drink/eskelapa/', counterEsKelapa),
    path('drink/minumankemasan/', counterMinumanKemasan),
    path('drink/minumansachet/', counterMinumanSachet),
    path('drink/yoshinoya/', counterYoshinoya),
]